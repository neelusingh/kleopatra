# translation of kwatchgnupg.po to Persian
# MaryamSadat Razavi <razavi@itland.ir>, 2006.
# Nasim Daniarzadeh <daniarzadeh@itland.ir>, 2006, 2007.
# Nazanin Kazemi <kazemi@itland.ir>, 2006.
msgid ""
msgstr ""
"Project-Id-Version: kwatchgnupg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-06 00:48+0000\n"
"PO-Revision-Date: 2007-01-01 10:32+0330\n"
"Last-Translator: Nasim Daniarzadeh <daniarzadeh@itland.ir>\n"
"Language-Team: Persian <kde-i18n-fa@kde.org>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "مریم سادات رضوی"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "razavi@itland.ir"

#: aboutdata.cpp:25
msgid "Steffen Hansen"
msgstr ""

#: aboutdata.cpp:25
msgid "Original Author"
msgstr "نویسنده اصلی"

#: aboutdata.cpp:30
#, kde-format
msgid "KWatchGnuPG"
msgstr ""

#: aboutdata.cpp:31
#, kde-format
msgid "GnuPG log viewer"
msgstr "مشاهده‌گر ثبت GnuPG"

#: aboutdata.cpp:32
#, kde-format
msgid "(c) 2004 Klarälvdalens Datakonsult AB\n"
msgstr ""

#: kwatchgnupgconfig.cpp:56
#, fuzzy, kde-format
#| msgid "Configure KWatchGnuPG"
msgctxt "@title:window"
msgid "Configure KWatchGnuPG"
msgstr "پیکربندی KWatchGnuPG"

#: kwatchgnupgconfig.cpp:72
#, kde-format
msgid "WatchGnuPG"
msgstr ""

#: kwatchgnupgconfig.cpp:82
#, kde-format
msgid "&Executable:"
msgstr "&قابل اجرا:‌"

#: kwatchgnupgconfig.cpp:91
#, kde-format
msgid "&Socket:"
msgstr "&سوکت:‌"

#: kwatchgnupgconfig.cpp:100
#, kde-format
msgid "None"
msgstr "هیچ‌کدام"

#: kwatchgnupgconfig.cpp:101
#, kde-format
msgid "Basic"
msgstr "پایه‌ای"

#: kwatchgnupgconfig.cpp:102
#, kde-format
msgid "Advanced"
msgstr "پیشرفته"

#: kwatchgnupgconfig.cpp:103
#, kde-format
msgid "Expert"
msgstr "متخصص"

#: kwatchgnupgconfig.cpp:104
#, kde-format
msgid "Guru"
msgstr "معلم"

#: kwatchgnupgconfig.cpp:105
#, kde-format
msgid "Default &log level:"
msgstr "سطح &ثبت پیش‌فرض:‌"

#: kwatchgnupgconfig.cpp:113
#, kde-format
msgid "Log Window"
msgstr "‌پنجره ثبت"

#: kwatchgnupgconfig.cpp:125
#, fuzzy, kde-format
#| msgctxt "history size spinbox suffix"
#| msgid " lines"
msgctxt "history size spinbox suffix"
msgid " line"
msgid_plural " lines"
msgstr[0] "خطوط"

#: kwatchgnupgconfig.cpp:126
#, kde-format
msgid "unlimited"
msgstr "نامحدود"

#: kwatchgnupgconfig.cpp:127
#, kde-format
msgid "&History size:"
msgstr "اندازه &تاریخچه:‌"

#: kwatchgnupgconfig.cpp:131
#, kde-format
msgid "Set &Unlimited"
msgstr "تنظیم &نامحدود‌"

#: kwatchgnupgconfig.cpp:142
#, kde-format
msgid "Enable &word wrapping"
msgstr "فعال‌سازی سطربندی &واژه‌"

#: kwatchgnupgmainwin.cpp:79
#, kde-format
msgid "[%1] Log cleared"
msgstr ""

#: kwatchgnupgmainwin.cpp:86
#, kde-format
msgid "C&lear History"
msgstr "&پاک کردن تاریخچه‌"

#: kwatchgnupgmainwin.cpp:117
#, kde-format
msgid "[%1] Log stopped"
msgstr ""

#: kwatchgnupgmainwin.cpp:133
#, fuzzy, kde-format
#| msgid ""
#| "The watchgnupg logging process could not be started.\n"
#| "Please install watchgnupg somewhere in your $PATH.\n"
#| "This log window is now completely useless."
msgid ""
"The watchgnupg logging process could not be started.\n"
"Please install watchgnupg somewhere in your $PATH.\n"
"This log window is unable to display any useful information."
msgstr ""
"فرایند ثبت watchgnupg را نمی‌توان آغاز کرد.\n"
"لطفاً، watchgnupg را در جای دیگری از $PATH خود بگذارید.\n"
"این پنجره در حال حاضر کاملاً بدون استفاده است."

#: kwatchgnupgmainwin.cpp:135
#, kde-format
msgid "[%1] Log started"
msgstr ""

#: kwatchgnupgmainwin.cpp:171
#, kde-format
msgid "There are no components available that support logging."
msgstr "هیچ محتوایی وجود ندارد که از ثبت پشتیبانی کند."

#: kwatchgnupgmainwin.cpp:178
#, kde-format
msgid ""
"The watchgnupg logging process died.\n"
"Do you want to try to restart it?"
msgstr ""
"فرایند ثبت watchgnupg مرد.\n"
"می‌خواهید آن را بازآغازی کنید؟"

#: kwatchgnupgmainwin.cpp:180
#, kde-format
msgid "Try Restart"
msgstr "سعی برای بازآغازی"

#: kwatchgnupgmainwin.cpp:181
#, kde-format
msgid "Do Not Try"
msgstr "سعی نکنید"

#: kwatchgnupgmainwin.cpp:183
#, kde-format
msgid "====== Restarting logging process ====="
msgstr "====== بازآغازی فرایند ثبت ====="

#: kwatchgnupgmainwin.cpp:187
#, fuzzy, kde-format
#| msgid ""
#| "The watchgnupg logging process is not running.\n"
#| "This log window is now completely useless."
msgid ""
"The watchgnupg logging process is not running.\n"
"This log window is unable to display any useful information."
msgstr ""
"فرایند ثبت watchgnupg اجرا نمی‌شود.\n"
"این پنجره ثبت در حال حاضر بدون استفاده است."

#: kwatchgnupgmainwin.cpp:222
#, kde-format
msgid "Save Log to File"
msgstr "ذخیره ثبت در پرونده"

#: kwatchgnupgmainwin.cpp:230
#, kde-format
msgid "Could not save file %1: %2"
msgstr ""

#. i18n: ectx: Menu (file)
#: kwatchgnupgui.rc:4
#, kde-format
msgid "&File"
msgstr ""

#. i18n: ectx: ToolBar (mainToolBar)
#: kwatchgnupgui.rc:13
#, kde-format
msgid "Main Toolbar"
msgstr ""

#: tray.cpp:29
#, kde-format
msgid "KWatchGnuPG Log Viewer"
msgstr "مشاهده‌گر ثبت KWatchGnuPG"

#~ msgid ""
#~ "The file named \"%1\" already exists. Are you sure you want to overwrite "
#~ "it?"
#~ msgstr ""
#~ "پرونده با نام »%1« قبلا وجود داشته است. می‌خواهید آن را جای‌نوشت کنید؟"

#~ msgid "Overwrite File"
#~ msgstr "جای‌نوشت پرونده"

#~ msgid "Configure KWatchGnuPG..."
#~ msgstr "پیکربندی KWatchGnuPG..."

#~ msgid "Overwrite"
#~ msgstr "جای‌نوشت"
