# Translation of kwatchgnupg.po to Ukrainian
# Copyright (C) 2004-2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Ivan Petrouchtchak <fr.ivan@ukrainian-orthodox.org>, 2004, 2005, 2007.
# Yuri Chornoivan <yurchor@ukr.net>, 2008, 2009, 2010, 2019.
msgid ""
msgstr ""
"Project-Id-Version: kwatchgnupg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-06 00:48+0000\n"
"PO-Revision-Date: 2019-07-24 08:14+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 19.07.70\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Іван Петрущак"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "ivanpetrouchtchak@yahoo.com"

#: aboutdata.cpp:25
msgid "Steffen Hansen"
msgstr "Steffen Hansen"

#: aboutdata.cpp:25
msgid "Original Author"
msgstr "Перший автор"

#: aboutdata.cpp:30
#, kde-format
msgid "KWatchGnuPG"
msgstr "KWatchGnuPG"

#: aboutdata.cpp:31
#, kde-format
msgid "GnuPG log viewer"
msgstr "GnuPG переглядач журналів"

#: aboutdata.cpp:32
#, kde-format
msgid "(c) 2004 Klarälvdalens Datakonsult AB\n"
msgstr "© Klarälvdalens Datakonsult AB, 2004\n"

#: kwatchgnupgconfig.cpp:56
#, kde-format
msgctxt "@title:window"
msgid "Configure KWatchGnuPG"
msgstr "Налаштування KWatchGnuPG"

#: kwatchgnupgconfig.cpp:72
#, kde-format
msgid "WatchGnuPG"
msgstr "WatchGnuPG"

#: kwatchgnupgconfig.cpp:82
#, kde-format
msgid "&Executable:"
msgstr "Файл &програми:"

#: kwatchgnupgconfig.cpp:91
#, kde-format
msgid "&Socket:"
msgstr "&Сокет:"

#: kwatchgnupgconfig.cpp:100
#, kde-format
msgid "None"
msgstr "Немає"

#: kwatchgnupgconfig.cpp:101
#, kde-format
msgid "Basic"
msgstr "Основний"

#: kwatchgnupgconfig.cpp:102
#, kde-format
msgid "Advanced"
msgstr "Додатковий"

#: kwatchgnupgconfig.cpp:103
#, kde-format
msgid "Expert"
msgstr "Експерт"

#: kwatchgnupgconfig.cpp:104
#, kde-format
msgid "Guru"
msgstr "Гуру"

#: kwatchgnupgconfig.cpp:105
#, kde-format
msgid "Default &log level:"
msgstr "Стандартний рівень &запису журналу:"

#: kwatchgnupgconfig.cpp:113
#, kde-format
msgid "Log Window"
msgstr "Вікно журналу"

#: kwatchgnupgconfig.cpp:125
#, kde-format
msgctxt "history size spinbox suffix"
msgid " line"
msgid_plural " lines"
msgstr[0] " рядок"
msgstr[1] " рядки"
msgstr[2] " рядків"
msgstr[3] " рядок"

#: kwatchgnupgconfig.cpp:126
#, kde-format
msgid "unlimited"
msgstr "необмежено"

#: kwatchgnupgconfig.cpp:127
#, kde-format
msgid "&History size:"
msgstr "Розмір &історії:"

#: kwatchgnupgconfig.cpp:131
#, kde-format
msgid "Set &Unlimited"
msgstr "&Не обмежувати"

#: kwatchgnupgconfig.cpp:142
#, kde-format
msgid "Enable &word wrapping"
msgstr "Ввімкнути &перенесення слів"

#: kwatchgnupgmainwin.cpp:79
#, kde-format
msgid "[%1] Log cleared"
msgstr "[%1] Журнал очищено"

#: kwatchgnupgmainwin.cpp:86
#, kde-format
msgid "C&lear History"
msgstr "О&чистити історію"

#: kwatchgnupgmainwin.cpp:117
#, kde-format
msgid "[%1] Log stopped"
msgstr "[%1] Журнал зупинено"

#: kwatchgnupgmainwin.cpp:133
#, kde-format
msgid ""
"The watchgnupg logging process could not be started.\n"
"Please install watchgnupg somewhere in your $PATH.\n"
"This log window is unable to display any useful information."
msgstr ""
"Не вдалось запустити процес ведення журналу watchgnupg.\n"
"Будь ласка, встановіть watchgnupg десь у вашому $PATH.\n"
"У цьому вікні журналу ви не знайдете корисних відомостей."

#: kwatchgnupgmainwin.cpp:135
#, kde-format
msgid "[%1] Log started"
msgstr "[%1] Журнал почато"

#: kwatchgnupgmainwin.cpp:171
#, kde-format
msgid "There are no components available that support logging."
msgstr "Немає компонентів, які підтримують ведення журналу."

#: kwatchgnupgmainwin.cpp:178
#, kde-format
msgid ""
"The watchgnupg logging process died.\n"
"Do you want to try to restart it?"
msgstr ""
"Процес ведення журналу watchgnupg зупинився.\n"
"Хочете спробувати його знов запустити?"

#: kwatchgnupgmainwin.cpp:180
#, kde-format
msgid "Try Restart"
msgstr "Спробувати перезапустити"

#: kwatchgnupgmainwin.cpp:181
#, kde-format
msgid "Do Not Try"
msgstr "Не пробувати"

#: kwatchgnupgmainwin.cpp:183
#, kde-format
msgid "====== Restarting logging process ====="
msgstr "== Перезапуск процесу ведення журналу =="

#: kwatchgnupgmainwin.cpp:187
#, kde-format
msgid ""
"The watchgnupg logging process is not running.\n"
"This log window is unable to display any useful information."
msgstr ""
"Процес ведення журналу watchgnupg не запущений.\n"
"У цьому вікні журналу ви не знайдете корисних відомостей."

#: kwatchgnupgmainwin.cpp:222
#, kde-format
msgid "Save Log to File"
msgstr "Зберегти журнал у файл"

#: kwatchgnupgmainwin.cpp:230
#, kde-format
msgid "Could not save file %1: %2"
msgstr "Не вдалося зберегти файл %1: %2"

#. i18n: ectx: Menu (file)
#: kwatchgnupgui.rc:4
#, kde-format
msgid "&File"
msgstr "&Файл"

#. i18n: ectx: ToolBar (mainToolBar)
#: kwatchgnupgui.rc:13
#, kde-format
msgid "Main Toolbar"
msgstr "Головний пенал"

#: tray.cpp:29
#, kde-format
msgid "KWatchGnuPG Log Viewer"
msgstr "Переглядач журналів KWatchGnuPG"

#~ msgid ""
#~ "The file named \"%1\" already exists. Are you sure you want to overwrite "
#~ "it?"
#~ msgstr ""
#~ "Файл з назвою «%1» вже існує. Ви впевнені, що хочете його перезаписати?"

#~ msgid "Overwrite File"
#~ msgstr "Перезаписати файл"
